INTRODUCTION
------------

This module reads the date of an image from its exif data, and then
tries to find the corresponding coordinates in the existing Track
nodes of the user, and populates a geofield with these coordinates.
The cardinality of the image is supported, but for this the cardinality
of the date field (Unix timestamp) and the geofield should be equal or higher
then the cardinality of the image fields. Currently only Unix timestamp date
fields are supported.
The Track content type is created by the GPX node creator module, so
that is a requirement for this module to work. The Track contains an offset
field, where you can set the difference between the clock of the camera and
the clock of the GPS device, if there is any.

CONFIGURATION
-------------

1. Add an Image field to some content type. Select the Enable GPX image
	checkbox. Make sure the "The maximum allowed image size" is not set,
	otherwise it causes loss of EXIF data. 
2. Add a Geofield to the same content type, its cardinality should not be
	less than that of the image field. Select the "Set as target of GPX image"
	checkbox.
3. Add a Date field (Unix timestamp) to the same content type. Make sure its
	cardinality is not less than that of the Image field. For 
	"Date attributes to collect" make sure the 'seconds' option is selected. 
	Select the "Use exif date from image" checkbox.
The module then tries to populate the date fields from the exif data of the
corresponding image, and the geofield from existing Track nodes created by
the current user. 

TIPS
------------

Applying the https://www.drupal.org/files/issues/leaflet_images-1.patch 
patch on the leaflet module, you can display the images as icons, and clicking
them you can display another image style. Displaying lots of images is not
is not really recommended.

KNOWN PROBLEMS
--------------

https://github.com/Leaflet/Leaflet/issues/724

