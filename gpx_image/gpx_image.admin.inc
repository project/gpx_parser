<?php

/**
 * @file
 * Configuration forms for the gpx_node_creator module.
 */

/**
 * Form that sets the directory containing the gpx files to create nodes from.
 */
function gpx_image_settings_form($form, &$form_state) {
  $form = array();
  $use_exif = variable_get('gpx_image_use_exif');

  $form['gpx_image_use_exif'] = array(
    '#type' => 'checkbox',
    '#title' => t('If available, use the coordinates from the exif data of the image.'),
    '#description' => t('When using mobile phones for taking pictures, the exif coordinates might be inaccurate compared to devices specially designed for tracking.'),
    '#default_value' => $use_exif,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  return $form;
}

/**
 * Submit function for the gpx_image_settings_form.
 */
function gpx_image_settings_form_submit($form, &$form_state) {
  $use_exif = variable_get('gpx_image_use_exif');
  if (isset($form_state['values']['gpx_image_use_exif'])) {
    if ($use_exif != $form_state['values']['gpx_image_use_exif']) {
      $use_exif = $form_state['values']['gpx_image_use_exif'];
      variable_set('gpx_image_use_exif', $use_exif);
    }
  }
}
