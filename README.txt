INTRODUCTION
------------

This module is an API that creates a GpxFile object from a GPX file. 
Calling the gpx_parser_parse_gpx_file($url) function returns a GpxFile object,
and all data of the file is easily accessible.