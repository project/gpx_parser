<?php
// module_load_include('inc', 'gpx_parser', 'gpx_formatters');
// module_load_include('inc', 'gpx_parser', 'gpx_parser_install_bundles');

module_load_include('inc', 'gpx_parser', 'includes/GpxParserWaypoint.class');
module_load_include('inc', 'gpx_parser', 'includes/GpxParserTrack.class');
module_load_include('inc', 'gpx_parser', 'includes/GpxParserTracksegment.class');
module_load_include('inc', 'gpx_parser', 'includes/GpxParserTrackpoint.class');
module_load_include('inc', 'gpx_parser', 'includes/GpxParserGpxFile.class');

//module_load_include('inc', 'gpx_parser', 'gpx_parser_efq');

/**
 * Implements hook_menu().
 */
function gpx_parser_menu() {
  $items['admin/config/gpx'] = array(
    'title' => 'GPX',
    'description' => 'Settings of GPX related modules.',
    'position' => 'left',
    'weight' => -10,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  return $items;
}

/**
 * Vincenty formula to calculates distance between two points.
 */
function gpx_parser_calculate_vincenty($lat1, $lon1, $lat2, $lon2) {
  $lat1 = deg2rad($lat1);
  $lat2 = deg2rad($lat2);
  $lon1 = deg2rad($lon1);
  $lon2 = deg2rad($lon2);
  $a = 6378137;
  $b = 6356752.314245;
  $f = 1 / 298.257223563;
  // WGS-84 ellipsoid.
  $londiff = $lon2 - $lon1;
  $u1 = atan((1 - $f) * tan($lat1));
  $u2 = atan((1 - $f) * tan($lat2));
  $sin_u1 = sin($u1);
  $cos_u1 = cos($u1);
  $sin_u2 = sin($u2);
  $cos_u2 = cos($u2);
  $lambda = $londiff;
  $lambda_p = 2 * M_PI;
  $iter_limit = 100;
  while (abs($lambda - $lambda_p) > 1e-12 && --$iter_limit > 0) {
    $sin_lambda = sin($lambda);
    $cos_lambda = cos($lambda);
    $sin_sigma = sqrt(($cos_u2 * $sin_lambda) * ($cos_u2 * $sin_lambda) + ($cos_u1 * $sin_u2 - $sin_u1 * $cos_u2 * $cos_lambda) * ($cos_u1 * $sin_u2 - $sin_u1 * $cos_u2 * $cos_lambda));
    if ($sin_sigma == 0) {
      return 0;
    }
    // Co-incident points.
    $cos_sigma = $sin_u1 * $sin_u2 + $cos_u1 * $cos_u2 * $cos_lambda;
    $sigma = atan2($sin_sigma, $cos_sigma);
    // Was atan2.
    $alpha = asin($cos_u1 * $cos_u2 * $sin_lambda / $sin_sigma);
    $cos_sq_alpha = cos($alpha) * cos($alpha);
    $cos2sigma_m = $cos_sigma - 2 * $sin_u1 * $sin_u2 / $cos_sq_alpha;
    $cc = $f / 16 * $cos_sq_alpha * (4 + $f * (4 - 3 * $cos_sq_alpha));
    $lambda_p = $lambda;
    $lambda = $londiff + (1 - $cc) * $f * sin($alpha) * ($sigma + $cc * $sin_sigma * ($cos2sigma_m + $cc * $cos_sigma * (-1 + 2 * $cos2sigma_m * $cos2sigma_m)));
  }
  if ($iter_limit == 0) {
    return FALSE;
  }
  // Formula failed to converge.
  $u_sq = $cos_sq_alpha * ($a * $a - $b * $b) / ($b * $b);
  $aa   = 1 + $u_sq / 16384 * (4096 + $u_sq * (-768 + $u_sq * (320 - 175 * $u_sq)));
  $bb   = $u_sq / 1024 * (256 + $u_sq * (-128 + $u_sq * (74 - 47 * $u_sq)));
  $delta_sigma = $bb * $sin_sigma * ($cos2sigma_m + $bb / 4 * ($cos_sigma * (-1 + 2 * $cos2sigma_m * $cos2sigma_m) -
  $bb / 6 * $cos2sigma_m * (-3 + 4 * $sin_sigma * $sin_sigma) * (-3 + 4 * $cos2sigma_m * $cos2sigma_m)));
  $s = $b * $aa * ($sigma - $delta_sigma);
  $s = round($s, 3);
  // Round to 1mm precision.
  return $s;
}


/**
 * The _gpx_parser_get_track() returns the GpxParserTrack object created from a DOMElement.
 */
function _gpx_parser_get_track($item) {
  $newdoc = new DOMDocument();
  $root = $newdoc->appendChild(new DOMElement('gpx', NULL, 'http://www.topografix.com/GPX/1/1'));
  $root->setAttribute('creator', 'gpx_parser');
  $root->setAttribute('version', '1.1');
  $root->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd');
  $cloned = $item->cloneNode(TRUE);
  $root->appendChild($newdoc->importNode($cloned,TRUE));
  $track = new GpxParserTrack();
  $newdoc->formatOutput = true;
  $track->gpxString = $newdoc->saveXML();
  $track->totalDistance = 0;
  $total_distance = 0;
  $d = 0;
  $track->name = check_plain($item->getElementsByTagName('name')->item(0)->nodeValue);
  $trkseg = $item->getElementsByTagName('trkseg');
  $tracksegments = $item->getElementsByTagName('trkseg');
  foreach ($tracksegments as $trackseg) {
    $trackpoints = array();
    $tracksegment = new GpxParserTracksegment();
    $trackpoints_dom_node_list = $trackseg->getElementsByTagName('trkpt');
    foreach ($trackpoints_dom_node_list as $item) {
      $trackpoints[] = $item;
    }
    $track->trackpointCount += count($trackpoints);
    $first_point = $trackpoints[0];
    $last_point = $trackpoints[count($trackpoints) - 1];
    if (isset($first_point->getElementsByTagName('time')->item(0)->nodeValue)) {
      $start_time = check_plain($first_point->getElementsByTagName('time')->item(0)->nodeValue);
      $start_timestamp = strtotime($start_time);
      $tracksegment->startTimestamp = $start_timestamp;
    }
    if (isset($last_point->getElementsByTagName('time')->item(0)->nodeValue)) {
      $end_time = check_plain($last_point->getElementsByTagName('time')->item(0)->nodeValue);
      $end_timestamp = strtotime($end_time);
      $tracksegment->endTimestamp = $end_timestamp;
    }
    if (isset($end_timestamp) && isset($start_timestamp)) {
      $tracksegment->duration = $end_timestamp - $start_timestamp;
    }
    if (isset($track->startTimestamp)) {
      $track->startTimestamp = $track->startTimestamp > $start_timestamp ? $start_timestamp : $track->startTimestamp;
    }
    else {
      if (isset($start_timestamp)) {
        $track->startTimestamp = $start_timestamp;
      }
    }
    if (isset($track->endTimestamp)) {
      $track->endTimestamp = $track->endTimestamp < $end_timestamp ? $end_timestamp : $track->endTimestamp;
    }
    else {
      if (isset($end_timestamp)) {
        $track->endTimestamp = $end_timestamp;
      }
    }
    $ele_array = array();

    for ($i = 0; $i < count($trackpoints); $i++) {
      $trackpt = $trackpoints[$i];
      $trackpoint = new GpxParserTrackpoint();
      $trackpoint->lat = (float) check_plain($trackpt->getAttribute('lat'));
      $trackpoint->lon = (float) check_plain($trackpt->getAttribute('lon'));
      $trackpoint->ele = check_plain($trackpt->getElementsByTagName('ele')->item(0)->nodeValue);
      $ele_array[] = $trackpoint->ele;
      $trackpoint->avgEle = _gpx_parser_get_averaged_item($ele_array, $i);
      if (isset($trackpt->getElementsByTagName('time')->item(0)->nodeValue)) {
        $trackpoint->time = check_plain($trackpt->getElementsByTagName('time')->item(0)->nodeValue);
        $trackpoint->timestamp = strtotime($trackpoint->time);
      }
      $tracksegment->trackpoints[] = $trackpoint;
      if ($i > 0) {
        $d = gpx_parser_calculate_vincenty($tracksegment->trackpoints[$i-1]->lat, $tracksegment->trackpoints[$i-1]->lon, $tracksegment->trackpoints[$i]->lat, $tracksegment->trackpoints[$i]->lon);
      }
      else {
        $d = 0;
      }
      $total_distance += $d;
      $dist[] = $total_distance;
      $trackpoint->currentDistance = $total_distance;
    }
    $tracksegment->totalDistance = $total_distance;
    $track->totalDistance += $total_distance;
    $track->tracksegments[] = $tracksegment;
  }
  return $track;
}

/**
 * The _gpx_parser_get_wpt() returns the GpxParserWaypoint object created from a DOMElement.
 */
function _gpx_parser_get_wpt($item) {
  $newdoc = new DOMDocument();
  $root = $newdoc->appendChild(new DOMElement('gpx', NULL, 'http://www.topografix.com/GPX/1/1'));
  $root->setAttribute('creator', 'gpx_parser');
  $root->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd');
  $root->setAttribute('version', '1.1');
  $cloned = $item->cloneNode(TRUE);
  $root->appendChild($newdoc->importNode($cloned,TRUE));
  $wpt = new GpxParserWaypoint();
  $newdoc->formatOutput = true;
  $wpt->gpxString = $newdoc->saveXML();
  $wpt->lat = (float) $item->getAttribute('lat');
  $wpt->lon = (float) $item->getAttribute('lon');
  $wpt->name = check_plain($item->getElementsByTagName('name')->item(0)->nodeValue);

  if (!isset($wpt->name)) {
    drupal_set_message(t('Cannot find "name" element.'), 'error');
  };
  $wpt->ele = check_plain($item->getElementsByTagName('ele')->item(0)->nodeValue);
  $wpt->cmt = check_plain($item->getElementsByTagName('cmt')->item(0)->nodeValue);
  $wpt->desc = check_plain($item->getElementsByTagName('desc')->item(0)->nodeValue);
  $wpt->sym = check_plain($item->getElementsByTagName('sym')->item(0)->nodeValue);
  $date = date_create_from_format('d-M-y G:i:s', $wpt->cmt);
  $wpt->timestamp = (int) date_timestamp_get($date);
  return $wpt;
}

/**
 * Parses the gpx file in the provided url into a GpxFile object, and returns that.
 */
function gpx_parser_parse_gpx_file($url) {
  $doc = new DOMDocument('1.0');
  if ($doc->load($url)) {
    $gpx_file = new GpxParserGpxFile();
    $wpt_node_list = $doc->getElementsByTagName('wpt');
    $trk_node_list = $doc->getElementsByTagName('trk');
    foreach ($wpt_node_list as $wpt_item) {
      $gpx_file->waypoints[] = _gpx_parser_get_wpt($wpt_item);
    }
    foreach ($trk_node_list as $trkitem) {
      $gpx_file->tracks[] = _gpx_parser_get_track($trkitem);
    }
    //drupal_set_message(t('File Uploaded'));
  }
  else {
    drupal_set_message(t('Cannot read file: !url', (array('!url' => $url))), 'error');
    return FALSE;
  }
  $gpx_file->uri = $url;
  return $gpx_file;
}
/**
 * Function to calculate average items from the 5 preceding item in an array.
 * GPS elevation data are not reliable, this helps to get more consequent data
 */
function _gpx_parser_get_averaged_item($input_array, $index) {
  // returns the average of the array item calculated from the 5 preceding items
  $count = count($input_array);
  $items = 0;
  $sum = 0;
  while ($index >= 0 && $items < 5) {
    $sum += $input_array[$index];
    $items ++;
    $index--;
  }
  $avg = $sum / $items;
  return $avg;
}
