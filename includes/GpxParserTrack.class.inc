<?php

class GpxParserTrack {

  public $name;
  public $tracksegments = array();
  public $trackpointCount;
  public $startTimestamp;
  public $endTimestamp;
  public $totalDistance;
  public $gpxString;
  public function toLinestring () {
    $segment_count = count($this->tracksegments);
    if ($segment_count) {
      $linestring = 'LINESTRING (';
      $current_segment = 0;
      while ($segment_count > $current_segment) {
        $current_point = 0;
        $segment_points = count($this->tracksegments[$current_segment]->trackpoints);
        while ($segment_points > $current_point) {
          if ($current_point > 0) {
            $linestring .= ', ';
          }
          $trackpoint = $this->tracksegments[$current_segment]->trackpoints[$current_point];
          if ($current_segment != 0 && $current_point != 0) {
            $linestring .= ', ';
          }
          $linestring .= floatval($trackpoint->lon) . ' '. floatval($trackpoint->lat);
          $current_point++;
        }
        $current_segment++;
      }
      $linestring .= ')';
      return $linestring;
    }
  }
  
  public function findCoordsByTime ($timestamp, $offset = 0) {
    // Most of the time there is only 1 tracksegment, so probably the
    // coordinates can be found there.
    if ($this->tracksegments[0]->endTimestamp >= $timestamp + $offset) {
      // Yes, it is there.
      $coords = $this->tracksegments[0]->findCoordinatesByTime($timestamp, $offset);
    }
    else {
      // The time falls out of the first tracksegment.
      $segment_count = count($this->tracksegments) - 1;
      // Let's pick a segment somewhere in the middle.
      $current_item = ceil($segment_count / 2);
      $coords = array();
      $low = 1;
      $high = $segment_count;
      while (empty($coords)) {
        while ($timestamp + $offset > $this->tracksegments[$current_item]->startTimestamp) {
          $low = $current_item;
          $current_item = $current_item + floor(($high - $current_item) / 2);
          if ($current_item == $low) {
            break;
          }
        }
        while ($timestamp + $offset < $this->tracksegments[$current_item]->endTimestamp) {
          $high = $current_item;
          $current_item = ceil(($current_item - $low) / 2) + $low;
          if ($current_item == $high) {
            break;
          }
        }
        if ($low + 1 >= $high || $timestamp + $offset == $this->tracksegments[$current_item]->endTimestamp || $timestamp + $offset == $this->tracksegments[$current_item]->startTimestamp) {
          $coords = $this->tracksegments[$current_item]->findCoordinatesByTime($timestamp, $offset);
        }
      }
    }
    return $coords;
  }

  public function toGeoJson($elevation = TRUE, $time = TRUE, $symbol = TRUE) {
    $segment_count = count($this->tracksegments);
    if ($segment_count) {
      $feature = array();
      $current_segment = 0;
      while ($segment_count > $current_segment) {
        $current_point = 0;
        $feature['geometry']['type'] = "LineString";
        $segment_points = count($this->tracksegments[$current_segment]->trackpoints);
        $feature['properties']['name'] = $this->name;
        $feature['properties']['distance'] = $this->totalDistance;
        while ($segment_points > $current_point) {
          $trackpoint = $this->tracksegments[$current_segment]->trackpoints[$current_point];
          $feature['geometry']['coordinates'][] = array(floatval($trackpoint->lon), floatval($trackpoint->lat));
          $feature['properties']['ele'][] = $trackpoint->ele;
          $feature['properties']['time'][] = $trackpoint->timestamp;
          $current_point++;
        }
        $current_segment++;
      }
    }
    return $feature;
  }

}