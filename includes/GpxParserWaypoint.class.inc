<?php

class GpxParserWaypoint {

  public $lat;
  public $lon;
  public $ele;
  public $name;
  public $cmt;
  public $desc;
  public $sym;
  public $timestamp;
  public $gpxString;
  public function toGeoJson($elevation = TRUE, $time = TRUE, $symbol = TRUE) {
    $feature = array();
    $feature['geometry'] = array(
    'type'        => 'Point',
    'coordinates' => array(floatval($this->lon), floatval($this->lat))
    );
    $feature['properties'] = array(
    'name' => $this->name,
    );
    if ($elevation && isset($this->ele)) {
      $feature['properties']['ele'] = $this->ele;
    }
    if ($time && isset($this->timestamp)) {
      $feature['properties']['time'] = $this->timestamp;
    }
    if ($symbol && isset($this->sym)) {
      $feature['properties']['sym'] = $this->sym;
    }
    return $feature;
  }

}