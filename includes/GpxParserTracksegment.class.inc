<?php

class GpxParserTracksegment {

  public $trackpoints = array();
  public $startTimestamp;
  public $endTimestamp;
  public $duration;
  public $totalDistance;

  // distance, duration, elevation data etc.
  public function getSampleData($size) {
    $sample = array();
    $step = ceil(count($this->trackpoints) / $size);
    if ($size > 0) {
      $i = 0;
      foreach ($this->trackpoints as $trackpoint) {
        $i++;
        if ($i == $step) {
          $i = 0;
          $sample['data'][] = $trackpoint;
        }
      }
    }
    return $sample;
  }

  public function findCoordinatesByTime($timestamp, $offset = 0) {
    $trackpoint_count = count($this->trackpoints) - 1;
    // Let's pick a segment somewhere in the middle.
    $current_item = floor($trackpoint_count / 2);
    $coords = array();
    $low = 0;
    $high = $trackpoint_count - 1;
          $counter = 0;
          $last = "";
    while (empty($coords)) {
      while ($timestamp + $offset > $this->trackpoints[$current_item]->timestamp) {
        // $timestamp is greater then the timestamp of the current item.
        // We set the lower limit.
        $low = $current_item;
        $current_item = $current_item + floor(($high - $current_item) / 2);
        if ($current_item <= ($low + 1)) {
          break;
        }
      }
      while ($timestamp + $offset < $this->trackpoints[$current_item]->timestamp) {
        $high = $current_item;
        $current_item = ceil(($current_item - $low) / 2) + $low;
        if ($current_item >= ($high - 1)) {
          break;
        }
      }
      if ($timestamp + $offset == $this->trackpoints[$current_item]->timestamp) {
        $coords = array($this->trackpoints[$current_item]->lon, $this->trackpoints[$current_item]->lat);
      }
      if ($low + 1 >= $high) {
        $coords = (abs($this->trackpoints[$high]->timestamp - $timestamp) > abs($this->trackpoints[$low]->timestamp - $timestamp)) ? array($this->trackpoints[$low]->lon, $this->trackpoints[$low]->lat)  : array($this->trackpoints[$high]->lon, $this->trackpoints[$high]->lat);
      }
    }
    return $coords;
  }

}