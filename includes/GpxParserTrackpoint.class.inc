<?php

class GpxParserTrackpoint {

  public $lat;
  public $lon;
  public $ele;
  public $time;
  public $timestamp;
  public $currentDistance;
  // The ele data are rather hectic, so just smoothe it out (count average of 5 preceding trackpoints).
  public $avgEle;

}

