<?php

class GpxParserGpxFile {

  public $uri;
  public $waypoints = array();
  public $tracks = array();

  public function toGeoJson($elevation = TRUE, $time = TRUE, $symbol = TRUE) {
    $feature_list = array();
    $feature_list['type'] = 'FeatureCollection';
    $feature_list['features'] = array();
    foreach ($this->waypoints as $wpt) {
      $feature['type'] = "Feature";
      $feature = array_merge($feature, $wpt->toGeoJson());
      $feature_list['features'][] = $feature;
    }
    foreach ($this->tracks as $track) {
      $feature['type'] = "Feature";
      $feature = array_merge($feature, $track->toGeoJson());
      $feature_list['features'][] = $feature;
    }
    return $feature_list;
  }

}