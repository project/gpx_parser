INTRODUCTION
------------

Garmin gps devices name waypoints with numbers. When you save these waypoints
to your mapsource gdb file or elsewhere, there will be name collisions.
In order to avoid name collisions, this module renames gpx waypoints in a
file field to YYYY-MM-DD-original_name-number format.


CONFIGURATION
-------------

Just upload the .gpx files to any filefield, and when you manage the fields
of the specific content type, you can select the Enable GPX rename checkbox.
The program automatically renames the already existing and newly added
waypoints in the file fields of this content type.

Alternatively you can upload gpx files to a directory, and specify this
directory under admin/config/gpx/gpx_rename.


KNOWN PROBLEMS AND LIMITATIONS
------------------------------

There is an issue with filenames containing national characters, so avoid them.
