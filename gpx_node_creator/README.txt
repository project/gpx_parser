INTRODUCTION
------------

This module creates a waypoint and a track content type,
and creates the waypoint and track nodes from gpx files.
If you attach a textfield to a vocabulary, and fill in the names
of gps symbols in these textfields, then the created waypoints will be
tagged according to their symbol.

CONFIGURATION
-------------

When managing the fields, the user can select a filefield of an
entity that holds a gpx file, and then the module creates waypoints
and tracks from this file. Alternatively you can upload gpx files to a
directory, and specify this directory under admin/config/gpx/gpx_node_creator.
When managing the fields of a taxonomy vocabulary, you can select the
"Tag waypoint nodes" checkbox for a textfield. After specifying the names of
gps symbols for the individual terms, this field will be used to tag the
created waypoints.
Note when uninstalling gpx node creator all the created waypoint and track nodes
will be deleted.

KNOWN PROBLEMS AND LIMITATIONS
------------------------------  

As of PHP 5.x it is not possible to read files that have national characters
in their names, so rename these files before uploading into a directory, or use
the transliteration module when uploading them as filefields.