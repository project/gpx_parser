<?php

/**
 * @file
 * Configuration forms for the gpx_node_creator module.
 */

/**
 * Form that sets the directory containing the gpx files to create nodes from.
 */
function gpx_node_creator_set_directory_form($form, &$form_state) {
  $form = array();
  $path = variable_get('gpx_node_creator_path');

  $form['gpx_node_creator_path'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#title' => t('Enter the directory that contains the gpx files to use to create nodes.'),
    '#description' => t('The path can be relative, for example public://gpx or it can be an absolute system path, for example f:\xampp\htdocs\drupal\sites\default\files\gpx'),
    '#default_value' => $path,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Process directory'),
  );
  return $form;
}

/**
 * Submit function for the gpx_rename_set_directory_form.
 */
function gpx_node_creator_set_directory_form_submit($form, &$form_state) {
  $path = variable_get('gpx_node_creator_path');
  if (isset($form_state['values']['gpx_node_creator_path'])) {
    if ($path != $form_state['values']['gpx_node_creator_path']) {
      $path = $form_state['values']['gpx_node_creator_path'];
      variable_set('gpx_node_creator_path', $path);
    }
    $files = file_scan_directory($path, '/.*\.gpx$/i');
    foreach ($files as $file) {
      if (!empty($file->uri)) {
        $uris[] = $file->uri;
      }
    }
    $step = 1;
    $batch = array(
    'operations' => array(
    array('gpx_node_creator_parse_files_batch',
    array($uris, $step))),
    'title' => t('Batch processing files'),
    'init_message' => t('Initializing'),
    'error_message' => t('An error occurred in the batch operation processing files.'),
    'progress_message' => t('Operation @current out of @total.'),
    'finished' => 'gpx_node_creator_files_finished_method',
    );
    batch_set($batch);
  }
}
